import random
import bitstring
import sys
import re
# for unit32 conversion
import numpy as np

# num of bits allocated to every cell
SYS_BITS = 64
BIT_NUM = 8
CELLS_POLLED = 6
NUM_OF_CELLS = 60
BATT_MAX = 4.000
BATT_MIN = 3.000
SOC_MAX = 100.0
SOC_MIN = 0.0

##########################################################################################
#                                Function Defintions                                     #
##########################################################################################

def calc_balanced_binaries(BIT_NUM):
    counter = 0
    cellID_array = []
    for i in range(2**BIT_NUM):
        num = bin(i)[2:]  # skip the starting two characters(i.e. 0b)

        # calculate all 1's from binary string
        num_of_ones = [ones for ones in num if ones == '1']

        if len(num_of_ones) == (BIT_NUM//2):  # floor division to get half of bits equal to zero
            counter = counter + 1
            # Pad num with zeros until it becomes of size of BIT_NUM
            num = num.zfill(BIT_NUM)

            #print(num)
            #print("The number of ones is: ", len(num_of_ones), " The actual binary number is: ", num)
            cellID_array.append(num)
            #print(cellID_array)

    #print("The possible binaries with equal ones and zeros are: ", counter)
    #print(cellID_array)
    #print(len(cellID_array))
    return(cellID_array)

def cyclicShift(input, bitsNum, dir):
    # covert number into list
    #temp = list(map(int, str(input)))
    temp = input
    size = len(temp)
    # print("the response is shifted by ", bitsNum," positions")

    if dir == '1': # cyclic right shift
        # slice list in two parts
        # put last part in front of the first part of the list
        out = temp[size-bitsNum : ] + temp[ : size-bitsNum]

    elif dir == '0': # cyclic left shift
        # slice list in two parts
        # put first part in front of the last part of the list
        out = temp[bitsNum : ] + temp[ : bitsNum ]

    else:
        print(" ERROR in function cyclicShift... ")
        return
    #print("1: Input is = ", temp)
    #print("2:   Out is = ", out)
    return out

def orBits(input, mask):
    BIT_NUM = len(bin(input)[2 : ])
    # bitwise OR between input and mask
    if len(str(input)) == len(str(mask)):
        print("The       input is: ", bin(input))
        print("The ORed output is: ", bin(input | mask))
        out = str(input | mask).zfill(BIT_NUM)
        print(out)
    else:
        print("Error in function orBits...")

def xorBits(input, mask):
    # bitwise XOR between input and mask
    # print("\nThe type of mask is: ", type(mask))
    # print("The         mask is: ", bin(mask))
    # print("The        input is: ", bin(input))
    out = bin(input ^ mask)
    # print("The XORed output is: ", out, "\n")
    #out = str(input ^ mask).zfill(BIT_NUM)

    return out

        #print(bin(int(out)))
    # else:
    #     print("\nSize mismatch : ")
    #     print(input)
    #     print(mask)
    #     print("Error in function xorBits...\n")

def translateChallenge(challenge):

    # Cell 1 and 2 are for the real time measurements
    Cell1 = challenge[0 :  round(SYS_BITS/8)] # [0:7]
    Cell2 = challenge[round(SYS_BITS/8): round((SYS_BITS/8)*2)]  # [8:15]

    # Cell 3 through 6 are for the authentications
    Cell3 = challenge[round((SYS_BITS/8)*2) : round((SYS_BITS/8)*3)]  # [16:23]
    Cell4 = challenge[round((SYS_BITS/8)*3) : round((SYS_BITS/8)*4)]  # [24:31]
    Cell5 = challenge[round((SYS_BITS/8)*4) : round((SYS_BITS/8)*5)]  # [32:39]
    Cell6 = challenge[round((SYS_BITS/8)*5) : round((SYS_BITS/8)*6)]  # [40:47]

    SLC = challenge[round((SYS_BITS/8)*6) : round((SYS_BITS/8)*7)]  # [48:55]
    XOR = challenge[round((SYS_BITS/8)*7) : ]  # [56:64]

    translation = [Cell1, Cell2, Cell3, Cell4, Cell5, Cell6, SLC, XOR]
    return translation

def convFloat2Bin(num):
    f1 = bitstring.BitArray(float=num, length=(int(SYS_BITS/8)))

    if (SYS_BITS == 32):
        # print(len(f1.bin))
        f1 = f1.bin[16:]
        # print("The real time measurement in binary is: ", f1, "with length: ", len(f1), "\n")
        return f1

    return f1.bin

def authCells(translation, cellID_array, authID_array, num):
    response = []
    # print("The translation array is: ", translation)
    for i in range(2, num):
        # print(i, ": ", translation[i])
        temp = cellID_array.index(translation[i])
        # print ("Temp is : ", temp )  # print the cell index for the authID array element
        response.append(authID_array[temp])

    # print("The cellID_array is :", cellID_array)
    # print("The authID_array is :", authID_array)
    # print("The response is: ", response)

    return response

def transform(R1, translation):

    #Print the initial response only for comparison
    R1_con = "".join(R1)
    # print("The concat R1 is: ", R1_con, ", with length: ", len(R1_con), " bits\n")

    #############################
    # Start the transformation
    #############################

    # print("The translation array is : ", translation)

    SLC = translation[6]
    SLC_val = int(SLC, 2)
    # print("SLC : ", SLC, ", the last bit (direction) is : ", SLC[-1], ", in decimal representation is: ", SLC_val, "\n")
    XOR = translation[7]
    # print(XOR)

    # make 64 bit XOR mask by extending the initial 8bit XOR
    XOR_mask = XOR+XOR+XOR+XOR+XOR+XOR+XOR+XOR
    # print("XOR mask is: ", XOR_mask, ", with length: ", len(XOR_mask))

    # make R1 from list to binary and XOR it with the mask
    # XOR
    R2 = xorBits(int("".join(R1).zfill(SYS_BITS),2), int(XOR_mask, 2))
    R2 = R2[2:].zfill(SYS_BITS)
    # print ("******R2 is: ", R2, "with length: ", len(R2))

    #temp = int(R2,2)
    #print("the value of temp is: ", temp, " the type is: ", type(temp), "in binary: ", R2, "with type: ", type(R2))

    # SHIFT
    # we divide by 4 because SLC_val(8bit=256) can become > 64=length of challenge
    # print("R2 before shift : ", R2, ", with length: ", len(R2[2:]), " bits")
    R2 = cyclicShift(R2, round(SLC_val/4), SLC[-1])
    # print("R2 after  shift : ", R2, ", with length: ", len(R2), " bits\n")

    return R2



#-------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------
# Outstation receives from master and performs authentication process

def mainLoop(temp_1, temp_2):
    # initialize both the cellID and the authentication array with balanced binaries
    cellID_array = calc_balanced_binaries(BIT_NUM)
    # the authID_array will change value with every new measurement
    authID_array = calc_balanced_binaries(BIT_NUM)
   
    # convert to double(float) to 32-bit integer
    temp_1 = np.uint32(temp_1)
    temp_2 = np.uint32(temp_2)

    # convert 32-bit integer to 32-bit binary string (so fcns can used them as is)
    temp_1 = '{:032b}'.format(temp_1)
    temp_2 = '{:032b}'.format(temp_2)
   
    # DEBUG: Printing to screen
    print("chall_first BINARY LIST: {}".format(temp_1))
    print("chall_second BINARY LIST: {}".format(temp_2))

    # join the strings
    challengeReceived = temp_1+temp_2
    print("challenge (temp) combined together: {}".format(challengeReceived))
   


    # YANNIS AND ME: DELETE THESE NUMBERS WHEN THE LOOP UNDER IS WORKING CORRECTLY
    # response_first = 705879
    # response_second = 8659789


    translation = translateChallenge(challengeReceived)
    print(translation)
    # print(len(translation))



    '''
    YANNIS, I NEED YOU TO WRITE THE AUTHENTICATION PART THAT NEEDS TO RUN IN THE
    OUTSTATION BELOW HERE.

    INPUTS: challengeReceived -> string in binary of the challenge sent by master

    ...... Authentication Process!................

    OUTPUT: reponse_first, reponse_second -> the two integers that are going to be sent
    to the master so the authentication can occur.


    IMPORTANT: I DON'T KNOW WHAT NEEDS TO BE UNCOMMENTED OUT FROM THE LOOP, SO PLEASE MAKE
    SURE THE PROCESS IS THE ONE THAT NEEDS TO RUN, SO WE CAN START TESTING THE FULL PROCESS

    '''


    '''
        R1a is going to be 32bits long and is going to be the FIRST part of the response (reponse_first)
        R1a contains the real-time voltage and SoC for Cell1 & Cell2
        each voltage and SoC is 8bits long so,
        R1a= [Cell1_voltage, Cell1_SoC, Cell2_voltage, Cell2_SoC] in bit format
        It is not an array, but put it like that here beacuse it is easier to explain
    '''
    # Measure real-time voltage and SoC for Cell 1 and Cell 2
    # Measurements (currently being passed manually for testing)
    '''
    vol_cells = [3.69,3.70]
    soc_cells = [56,57] 
    
    vol_cells = [3.28,3.30]
    soc_cells = [20,22] 

    vol_cells = [3.44,3.45]
    soc_cells = [20,21] 
   
    '''
    vol_cells = [3.75,3.76]
    soc_cells = [54,55] 

    R1a = []
    for count in range(2):
        #real_time_measurement = round(random.uniform(BATT_MIN, BATT_MAX), 2)
        real_time_measurement = vol_cells[count]
        
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("REAL-TIME MEASUREMENT: {}".format(real_time_measurement))
        real_time_measurement = int(real_time_measurement * (10 ** 2))
        print("REAL-TIME MEASUREMENT INT: {}".format(real_time_measurement))

        # print(real_time_measurement)
        # print(bin(real_time_measurement)[2:])
        R1a.append(bin(real_time_measurement)[2:])  # this voltage value is always 9bit long (0-512)
        print("R1a: {}".format(R1a))

        #real_time_soc = round(random.uniform(SOC_MIN, SOC_MAX))
        real_time_soc = soc_cells[count]
        print("Real Time SOC: {}".format(real_time_soc))

        
        # print(real_time_soc)
        # print(bin(real_time_soc)[2:].zfill(BIT_NUM-1)) # this SoC percent value is always 7bit long (0-128)
        R1a.append(bin(real_time_soc)[2:].zfill(BIT_NUM - 1))  # this SoC percent value is always 7bit long (0-128)
        print("R1a seconds append: {}".format(R1a))
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")


    # print("R1a is : ", R1a, "with length: ", len(R1a), "\n")

    '''
        R1b is going to be 32bits long and is going to be the SECOND part of the response (reponse_second)
        R1b contains the authID_array values for Cell3 & Cell4 & Cell5 & Cell6
        each authID_array value is 8bits long so,
        R1b= [authID_array_value_forCell3, authID_array_value_forCell4, authID_array_value_forCell5, authID_array_value_forCell6] in bit format
        It is not an array, but put it like that here beacuse it is easier to explain
    '''
    R1b = authCells(translation, cellID_array, authID_array, CELLS_POLLED)
    # # print("R1b is : ", R1b, "with length: ", len(R1b), "\n")

    '''
        I concatenate R1a and R1b beacuse this will be shifted and XORed before the final response is calculated
        Reponse_first & reponse_second will be the shifted and XORed versions of R1a and R1b
        This happens in the transform(R1, translation) function
        It is part of the encryption of the DERauth protocol
    '''
    R1 = R1a + R1b
    # # print("R1 is : ", R1, "with length: ", len(R1), "\n")


    R2 = transform(R1, translation)
    # # print("R2 is : ", R2, ", with length: ", len(R2), "\n")

    # Just a copy of the final response
    # In essense R2 = response = reponse_first + reponse_second (in bit-string representation)
    response = R2



    # The first 32bits of the response are the response_first portion
    # Following I convert it to integer
    response_first  = response[0:16]
    # print("response_first is : ", response_first, ", with length: ", len(response_first), "\n")
    response_first = np.uint32(int(response_first,2))
    # print("Response_first is : ", response_first, ", with type: ", type(response_first), "\n")

    # The last 32bits of the response are the response_second portion
    # Following I convert it to integer
    response_second = response[16:32]
    # print("response_second is : ", response_second, ", with length: ", len(response_second), "\n")
    response_second = np.uint32(int(response_second,2))
    # print("response_first is : ", response_second, ", with type: ", type(response_first), "\n")



    response_third = response[32:48]
    response_third = np.uint32(int(response_third,2))

    response_fourth = response[48:64]
    response_fourth = np.uint32(int(response_fourth,2))



    '''
        The following update part should happen in BOTH the Master and Outstation devices
        This enhances the protocol entropy and makes it more difficult to crack it
        Let's leave it for now and just make the most "basic" implementation for illustration purposes
    '''

    # UPDATE AuthDArrays in Master using real-time voltage measurements from cells 1 and 2
    # Update AuthID_array with real time measurements (voltages and SoCs)
    # real_time_bin_measurement = "".join(R1a).zfill(32)
    # print(" The real_time_bin_measurement is : ", real_time_bin_measurement)
    # authID_array = updateAuthID_array(authID_array, real_time_bin_measurement)



    return response_first, response_second, response_third, response_fourth

##########################################################################################
#                           END of Function Defintions                                   #
##########################################################################################



