# derauth_pydnp3

DERauth implementation using pydnp3 (based on opendp3) - Master device.

Based on:

1) pydnp3: https://github.com/ChargePoint/pydnp3.git
2) DERauth

Dependencies of pydnp3:
1) pybind11
2) opendnp3 (C++)/Automak


Installation dependencies:
---------------------------

1) cmake: 

sudo apt install cmake

2) sqlite3:

sudo apt install sqlite3

3) pip3:

sudo apt install python3-pip

4) pip3:

pip3 install bitstring

pip3 install numpy

Important:
-----------

- Clone the respository recursively

git clone --recursive https://......

OR


- After cloning: initialize the submodules(git repositories that are dependecies) use the following command:

1) Add the main submodule using
git submodule add https://github.com/ChargePoint/pydnp3.git 

2) Inititialize other submodules (inside main submodule) as:
git submodule update --init --recursive


Install pydnp3:
----------------

$ cd pydnp3

$ sudo python3 setup.py install
