import random
import bitstring
import sys
import re

# needed for bit to double conversion
import numpy as np

# Constants Definitions
SYS_BITS = 64 # num of bits allocated to every cell
BIT_NUM = 8
CELLS_POLLED = 6
NUM_OF_CELLS = 60
BATT_MAX = 4.000
BATT_MIN = 3.000
SOC_MAX = 100.0
SOC_MIN = 0.0
MULTIPLIER = (2**((2*BIT_NUM)-5))



def calc_balanced_binaries(BIT_NUM):
    counter = 0
    cellID_array = []
    for i in range(2**BIT_NUM):
        num = bin(i)[2:]  # skip the starting two characters(i.e. 0b)

        # calculate all 1's from binary string
        num_of_ones = [ones for ones in num if ones == '1']

        if len(num_of_ones) == (BIT_NUM//2):  # floor division to get half of bits equal to zero
            counter = counter + 1
            # Pad num with zeros until it becomes of size of BIT_NUM
            num = num.zfill(BIT_NUM)

            #print(num)
            #print("The number of ones is: ", len(num_of_ones), " The actual binary number is: ", num)
            cellID_array.append(num)
            #print(cellID_array)

    #print("The possible binaries with equal ones and zeros are: ", counter)
    #print(cellID_array)
    #print(len(cellID_array))
    return(cellID_array)

def cyclicShift(input, bitsNum, dir):
    # covert number into list
    #temp = list(map(int, str(input)))
    temp = input
    size = len(temp)
    # print("the response is shifted by ", bitsNum," positions")

    if dir == '1': # cyclic right shift
        # slice list in two parts
        # put last part in front of the first part of the list
        out = temp[size-bitsNum : ] + temp[ : size-bitsNum]

    elif dir == '0': # cyclic left shift
        # slice list in two parts
        # put first part in front of the last part of the list
        out = temp[bitsNum : ] + temp[ : bitsNum ]

    else:
        print(" ERROR in function cyclicShift... ")
        return
    #print("1: Input is = ", temp)
    #print("2:   Out is = ", out)
    return out

def orBits(input, mask):
    BIT_NUM = len(bin(input)[2 : ])
    # bitwise OR between input and mask
    if len(str(input)) == len(str(mask)):
        print("The       input is: ", bin(input))
        print("The ORed output is: ", bin(input | mask))
        out = str(input | mask).zfill(BIT_NUM)
        print(out)
    else:
        print("Error in function orBits...")

def xorBits(input, mask):
    # bitwise XOR between input and mask
    # print("\nThe type of mask is: ", type(mask))
    # print("The         mask is: ", bin(mask))
    # print("The        input is: ", bin(input))
    out = bin(input ^ mask)
    # print("The XORed output is: ", out, "\n")
    #out = str(input ^ mask).zfill(BIT_NUM)

    return out

        #print(bin(int(out)))
    # else:
    #     print("\nSize mismatch : ")
    #     print(input)
    #     print(mask)
    #     print("Error in function xorBits...\n")

def buildChallenge(cellNums, cellID_array):
    challenge = []
    MAX = 2**BIT_NUM - 1
    MIN = 0 # XOR will always give an 8bit mask

    for i in range(len(cellNums)):
        # print(i)
        # print(cellNums[i])
        # print(int(cellNums[i]))
        temp = cellID_array[int(cellNums[i])]
        # print(temp)
        # print("\n")
        challenge.append(temp.zfill(BIT_NUM))


    # Calculate the value that the challenge will be SHIFTED (with size of BIT_NUM = 8bits)
    # SLC mask will always start with 1 and have length of (BIT_NUM/2)
    SLC = bin(random.randint(MIN, MAX))[2 :]
    # print(len(SLC), " : ", SLC)
    challenge.append(SLC.zfill(BIT_NUM))   # make the size BIT_NUM

    # Calculate the value that the challenge will be XORed against(with size of BIT_NUM)
    XOR = bin(random.randint(MIN, MAX))[2 :]
    #print(len(XOR), " : ", XOR)
    challenge.append(XOR.zfill(BIT_NUM))  # make the size BIT_NUM


    #############################
    #       CHALLENGE           #
    #############################
    challenge = ''.join(challenge)
    #print("\nThe challenge is: ", challenge, ", with length: ", len(challenge), " bits \n" )
    return challenge

def translateChallenge(challenge):

    # Cell 1 and 2 are for the real time measurements
    Cell1 = challenge[0 :  round(SYS_BITS/8)] # [0:7]
    Cell2 = challenge[round(SYS_BITS/8): round((SYS_BITS/8)*2)]  # [8:15]

    # Cell 3 through 6 are for the authentications
    Cell3 = challenge[round((SYS_BITS/8)*2) : round((SYS_BITS/8)*3)]  # [16:23]
    Cell4 = challenge[round((SYS_BITS/8)*3) : round((SYS_BITS/8)*4)]  # [24:31]
    Cell5 = challenge[round((SYS_BITS/8)*4) : round((SYS_BITS/8)*5)]  # [32:39]
    Cell6 = challenge[round((SYS_BITS/8)*5) : round((SYS_BITS/8)*6)]  # [40:47]

    SLC = challenge[round((SYS_BITS/8)*6) : round((SYS_BITS/8)*7)]  # [48:55]
    XOR = challenge[round((SYS_BITS/8)*7) : ]  # [56:64]

    translation = [Cell1, Cell2, Cell3, Cell4, Cell5, Cell6, SLC, XOR]
    return translation

def convFloat2Bin(num):
    f1 = bitstring.BitArray(float=num, length=(int(SYS_BITS/8)))

    if (SYS_BITS == 32):
        # print(len(f1.bin))
        f1 = f1.bin[16:]
        # print("The real time measurement in binary is: ", f1, "with length: ", len(f1), "\n")
        return f1

    return f1.bin


def authCells(translation, cellID_array, authID_array, num):
    response = []
    # print("The translation array is: ", translation)
    for i in range(2, num):
        # print(i, ": ", translation[i])
        temp = cellID_array.index(translation[i])
        # print ("Temp is : ", temp )  # print the cell index for the authID array element
        response.append(authID_array[temp])

    # print("The cellID_array is :", cellID_array)
    # print("The authID_array is :", authID_array)
    # print("The response is: ", response)

    return response

def transform(R1, translation):

    #Print the initial response only for comparison
    R1_con = "".join(R1)
    # print("The concat R1 is: ", R1_con, ", with length: ", len(R1_con), " bits\n")

    #############################
    # Start the transformation
    #############################

    # print("The translation array is : ", translation)

    SLC = translation[6]
    SLC_val = int(SLC, 2)
    # print("SLC : ", SLC, ", the last bit (direction) is : ", SLC[-1], ", in decimal representation is: ", SLC_val, "\n")
    XOR = translation[7]
    # print(XOR)

    # make 64 bit XOR mask by extending the initial 8bit XOR
    XOR_mask = XOR+XOR+XOR+XOR+XOR+XOR+XOR+XOR
    # print("XOR mask is: ", XOR_mask, ", with length: ", len(XOR_mask))

    # make R1 from list to binary and XOR it with the mask
    # XOR
    R2 = xorBits(int("".join(R1).zfill(SYS_BITS),2), int(XOR_mask, 2))
    R2 = R2[2:].zfill(SYS_BITS)
    # print ("******R2 is: ", R2, "with length: ", len(R2))

    #temp = int(R2,2)
    #print("the value of temp is: ", temp, " the type is: ", type(temp), "in binary: ", R2, "with type: ", type(R2))

    # SHIFT
    # we divide by 4 because SLC_val(8bit=256) can become > 64=length of challenge
    # print("R2 before shift : ", R2, ", with length: ", len(R2[2:]), " bits")
    R2 = cyclicShift(R2, round(SLC_val/4), SLC[-1])
    # print("R2 after  shift : ", R2, ", with length: ", len(R2), " bits\n")

    return R2

def revTranform(R3, translation):
    #############################
    #       RESPONSE            #
    #############################
    # Print the initial response only for comparison
    # print("The response is : ", R3, ", with length: ", len(R3), " bits\n")

    #############################
    # Start the transformation  #
    #############################

    SLC = translation[6]
    SLC_val = int(SLC, 2)
    # print("SLC : ", SLC, ", the last bit (direction) is : ", SLC[-1], ", in decimal representation is: ", SLC_val, "\n")
    XOR = translation[7]

    # Initialize R2_hat
    R2_hat = R3

    # SHIFT
    # we divide by 10 because SLC_val can become > 64=length of challenge
    # print("R2 before shift : ", R2_hat, ", with length: ", len(R2_hat), " bits")

    if SLC[-1] == '0':
        dir = '1'
    else:
        dir = '0'

    R2_hat = cyclicShift(R2_hat, round(SLC_val/4), dir)
    # print("R2 after  shift : ", R2_hat, ", with length: ", len(R2_hat), " bits\n")

    # make 64 bit XOR mask by extending the initial 8bit XOR
    XOR_mask = XOR+XOR+XOR+XOR+XOR+XOR+XOR+XOR
    # print("XOR mask is: ", XOR_mask, ", with length: ", len(XOR_mask))

    # XOR
    # make R1 from list to binary and XOR it with the mask
    R1_hat = xorBits(int("".join(R2_hat).zfill(SYS_BITS),2), int(XOR_mask, 2))[2:]
    R1_hat = "".join(R1_hat).zfill(SYS_BITS)
    # print("Then  R1_hat is : ", R1_hat, ", with length: ", len(R1_hat), " bits\n")

    return R1_hat

def validateResponse(challenge, response, cellID_array, authID_array):
    flag = 0
    print("**** The challenge is : ", challenge)
    print("**** The response is  : ", response)

    # slice challenge
    ind1 = cellID_array.index(challenge[16:24])
    ind2 = cellID_array.index(challenge[24:32])
    ind3 = cellID_array.index(challenge[32:40])
    ind4 = cellID_array.index(challenge[40:48])

    # slice response
    Cell1 = int(response[round(SYS_BITS/2):round(SYS_BITS/2)+round(SYS_BITS/8)],2)                     # response[32:39]
    Cell2 = int(response[round(SYS_BITS/2)+round(SYS_BITS/8): round(SYS_BITS/2)+round(SYS_BITS/4)],2)  # response[40:47]
    Cell3 = int(response[round(SYS_BITS/2)+round(SYS_BITS/4):round(SYS_BITS/2)+round(SYS_BITS/8)*3],2) # response[48:55]
    Cell4 = int(response[round(SYS_BITS/2)+round(SYS_BITS/8)*3: ],2)                                   # response[56:64]

    # print("The authID_array[ind1] is: ",  authID_array[ind1], ", The Cell1 is: ", bin(Cell1)[2:])
    # print("The authID_array[ind1] is: ", int(authID_array[ind1],2), ", The Cell1 is: ", Cell1)
    # print("The authID_array[ind2] is: ",  authID_array[ind2], ", The Cell2 is: ", bin(Cell2)[2:])
    if (int(authID_array[ind1],2) == Cell1 and int(authID_array[ind2],2) == Cell2 and int(authID_array[ind3],2) == Cell3 and int(authID_array[ind4],2) == Cell4):
        flag = 1

    # print("Flag is: ", flag)
    return flag

def updateAuthID_array(authID_array, measurement):
    # print(measurement)
    mask = measurement[0: int(SYS_BITS/8)] # [0:8]
    # print("The mask is: ", mask, " with type: ", type(bin(int(mask,2))), "\n")

    for iter in range(len(authID_array)):
        # print(authID_array[iter])
        authID_array[iter] = xorBits(int(authID_array[iter], 2), int(mask,2))[2:]
        # print(authID_array[iter])
        # print("\n")

    return authID_array


#####################################################################################################################
#       Program Execution
#####################################################################################################################

# Create a class called MasterDERAuth that can perform this operation (some attributes need to be shared)

class MasterDERAuth:
    def __init__(self):
        # initialize both the cellID and the authentication array with balanced binaries
        # attr 1
        self.cellID_array = calc_balanced_binaries(BIT_NUM)

        # attr 2
        self.challenge = self.initChallenge()

    
        # the authID_array will change value with every new measurement
        # attr 3
        self.authID_array = calc_balanced_binaries(BIT_NUM)


    def initChallenge(self):
        # Initialize the challenge
        # array to indicate which cell are going to be measured and authenticated
        cell1_index = random.randint(1, NUM_OF_CELLS)
        cell2_index = random.randint(1, NUM_OF_CELLS)
        cell3_index = random.randint(1, NUM_OF_CELLS)
        cell4_index = random.randint(1, NUM_OF_CELLS)
        cell5_index = random.randint(1, NUM_OF_CELLS)
        cell6_index = random.randint(1, NUM_OF_CELLS)
        cellNums = [str(cell1_index), str(cell2_index), str(cell3_index), str(cell4_index), str(cell5_index), str(cell6_index)]
        init_chal = buildChallenge(cellNums, self.cellID_array) # just a placeholder for the challenge
        return init_chal 


    # calculate the challenge 
    def caculateAuthChallenge(self):
       
        # Calculate new challenge
        self.challenge = self.initChallenge()
        
        print("cellID_array: {}".format(self.cellID_array))    
        print("challenge (64-bits): {}".format(self.challenge))
        print("length of challenge: {}".format(len(self.challenge)))
    
        # Splitting string into equal halves 
        challenge_first, challenge_second = self.challenge[:len(self.challenge)//2],self.challenge[len(self.challenge)//2:] 

        # challenge in integer version (to be sent through DNP3)
        print("challenge_first bits: {}".format(challenge_first))
        print("challenge_second bits: {}".format(challenge_second))
        challenge_first = np.uint32(int(challenge_first,2))
        challenge_second = np.uint32(int(challenge_second,2))
        print("challenge_first int: {}".format(challenge_first))
        print("challenge_second int: {}".format(challenge_second))
        return challenge_first, challenge_second


    # decodes the response from outstation and authenticates
    def decodeAuthResponse(self, R2_first, R2_second, R2_third, R2_fourth):
        # R2 - response from DER Outstation

        # convert to from double (float) to integer
        R2_first = np.uint32(R2_first)
        R2_second = np.uint32(R2_second)
        R2_third = np.uint32(R2_third)
        R2_fourth = np.uint32(R2_fourth)

        # convert 32-bit integer to 32-bit binary
        R2_first = '{:016b}'.format(R2_first)
        R2_second = '{:016b}'.format(R2_second)
        R2_third = '{:016b}'.format(R2_third)
        R2_fourth = '{:016b}'.format(R2_fourth)

        print("R2_first BINARY LIST: {}".format(R2_first))
        print("R2_second BINARY LIST: {}".format(R2_second))
        print("R2_third BINARY LIST: {}".format(R2_third))
        print("R2_fourth BINARY LIST: {}".format(R2_fourth))

        # join the strings
        R2 = R2_first+R2_second+R2_third+R2_fourth
        print("R2 combined together: {}".format(R2))

        # Continue process
        translation = translateChallenge(self.challenge)
        R1_hat = revTranform(R2, translation)

        # # Check that R1 and R1_hat are exactly the same
        # temp1 = "".join(R1).zfill(SYS_BITS)
        # print(temp1)
        # temp2 = R1_hat
        # print(temp2)

        if validateResponse(self.challenge, R1_hat, self.cellID_array, self.authID_array):
            print("\n\nAuthentication Succeeded!!! \n\n")

        '''
            The following update part should happen in BOTH the Master and Outstation devices
            This enhances the protocol entropy and makes it more difficult to crack it
            Let's leave it for now and just make the most "basic" implementation for illustration purposes
        '''

        # UPDATE AuthDArrays in Master using real-time voltage measurements from cells 1 and 2
        # Update AuthID_array with real time measurements (voltages and SoCs)
        # real_time_bin_measurement = "".join(R1a).zfill(32)
        # print(" The real_time_bin_measurement is : ", real_time_bin_measurement)
        # authID_array = updateAuthID_array(authID_array, real_time_bin_measurement)

